﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcAnimations : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_BOOL = "Idle";
    private const string TALK_BOOL = "Talk";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_BOOL);
    }

    public void AnimateTalk()
    {
        Animate(TALK_BOOL);
    }

    #endregion

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }

    void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach (AnimatorControllerParameter parameter in animator.parameters)
        {
            if (parameter.name != animation) animator.SetBool(parameter.name, false);
        }
    }
}
