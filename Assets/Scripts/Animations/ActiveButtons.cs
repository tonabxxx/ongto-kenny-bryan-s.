﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveButtons : MonoBehaviour
{
    public GameObject[] buttons;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PressPlayerButton()
    {
        foreach (GameObject button in buttons)
        {
            button.gameObject.SetActive(false);
        }

        buttons[0].gameObject.SetActive(true);
    }

    public void PressMonstersButton()
    {
        foreach (GameObject button in buttons)
        {
            button.gameObject.SetActive(false);
        }

        buttons[1].gameObject.SetActive(true);
    }

    public void PressNpcButton()
    {
        foreach (GameObject button in buttons)
        {
            button.gameObject.SetActive(false);
        }

        buttons[2].gameObject.SetActive(true);
    }
}
