﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimationController : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_BOOL = "Idle";
    private const string WALK_BOOL = "Walk";
    private const string RUN_BOOL = "Run";
    private const string ATTACK_BOOL = "Attack";
    private const string DAMAGE_BOOL = "Damage";
    private const string SKILL_BOOL = "Skill";
    private const string DEAD_BOOL = "Dead";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_BOOL);
    }

    public void AnimateWalk()
    {
        Animate(WALK_BOOL);
    }

    public void AnimateRun()
    {
        Animate(RUN_BOOL);
    }

    public void AnimateAttack()
    {
        Animate(ATTACK_BOOL);
    }

    public void AnimateDamage()
    {
        Animate(DAMAGE_BOOL);
    }

    public void AnimateSkill()
    {
        Animate(SKILL_BOOL);
    }

    public void AnimateDead()
    {
        Animate(DEAD_BOOL);
    }

    #endregion

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }

    void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach (AnimatorControllerParameter parameter in animator.parameters)
        {
            if (parameter.name != animation) animator.SetBool(parameter.name, false);
        }
    }
}
