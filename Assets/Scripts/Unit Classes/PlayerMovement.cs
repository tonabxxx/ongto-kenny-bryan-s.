﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    private NavMeshAgent agent;

    public Animator animator;
    private PlayerAnimationController animationController;

    public float moveSpeed = 10;
    public float rotSpeed = 5;

    public GameObject marker;
    public Vector3 offset = new Vector3(0, 0, 0);

    private Vector3 targetPosition;

    private bool isIdle;
    private bool isMovingToMarker;

    private Vector3 lookAtTarget;
    private Quaternion playerRot;

    private GameObject point;

    public float movementSpeed;

    private bool isStopped = false;

    // Use this for initialization
    void Start ()
    {
        animationController = GetComponent<PlayerAnimationController>();

        targetPosition = transform.position;

        isMovingToMarker = false;
        isIdle = false;

        agent = this.GetComponent<NavMeshAgent>();

        agent.speed = moveSpeed;
    }
	
	// Update is called once per frame
	void Update ()
    {
        ClickToMove();
        if (isIdle)
        {
            agent.isStopped = true;
            animationController.AnimateIdle();
        }

        else if (isMovingToMarker)
        {
            agent.isStopped = false;
            animationController.AnimateNormalRun();
            MoveToMarker();
        }
	}

    void ClickToMove()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isIdle = false;
            Destroy(point);
            SetTargetPosition();
        }
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.transform != null)
            {
                if (hit.transform.gameObject.tag == "Ground")
                {
                    MoveToPosition(hit);
                }
            }
        }
    }

    void MoveToPosition(RaycastHit hit)
    {
        targetPosition = hit.point;

        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
                                   transform.position.y,
                                   targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        isMovingToMarker = true;

        if (hit.transform.gameObject.tag == "Ground")
        {           
            point = Instantiate(marker, targetPosition + offset, transform.rotation);
        }
    }

    void MoveToMarker()
    {
        
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);

        agent.SetDestination(targetPosition);

        if (Vector3.Distance(transform.position, targetPosition) <= 0.5f)
        {
            //Debug.Log("I arrived");
            Destroy(point);
            isMovingToMarker = false;
            isIdle = true;
        }
    }
}
